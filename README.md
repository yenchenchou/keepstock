# Keep Stock Web API

**The repository provide resource to setup Keep Stock Recommendation API through local laptop. Also, it provides requirement resource if deployed in vertual machine(GPU Box - linux) and AWS for scalable testing**

![demo page](data/img/demo.png)

# Getting Started

## Installation - Local Machine / Remote Server (GPU Box) (Waiting for GPU box to be repaired)
1. Clone this repo
2. Direct to folder `cd keepstock_gpubox`
3. Run `docker-compose.yml up`
4. Direct the page to :
    * Local Machine: `http://127.0.0.1:8000/docs` and Done! 
    * Remote Server (GPU Box): `http://10.42.162.210:56734/docs` and Done! 

# Resource Requirements

## 1. Local Machine / Remote Server (GPU Box)
### API/Web Demo Setup 
1. Python 3.6+, FASTApi, Uvicorn
2. Docker
3. Kubernetes (Optional)
4. If web Demo Included
    1. React / Bootstrap (Optional)
    2. Node.js (Optional)

## 2. Remote Server (AWS)
### API/Web Demo Setup (Solution 1)
1. SageMaker (Python 3.6+, FASTApi, Uvicorn, Docker, Kubernetes, Airflow)
2. ASW Lambda
3. AWS API Gateway
4. AWS S3
5. If web Demo Included
    1. EC2 / AWS Elastic Beanstalk (Optional): React / Bootstrap / Node.js

### API/Web Demo Setup (Solution 2 - introduced by MLops team)
![mlops](data/img/TigerTeam_MLOps.png)


# I/O Format
* Input Format: SAP Account ID in digits for example `800000000` or `0800000000` as well as the number of SKUs you prefered to receive
* Output: json format respond, for example:

```json
{
  "3Q021": "Safety Knife,5-7/8 in.,Orange",
  "30E657": "Quick Connect,Plug,1/4\\\" Body,1/4\\\"-18",
  "13U884": "Cut-Resistant Gloves,Size 9,PR",
  "33TW12": "Heat Resistant Sleeve,Terrycloth,14 In",
  "1LMC2": "Black Pipe Nipple,Threaded,1/2x1-1/2 In",
  "3BC92": "Chemical Resistant Glove,14\\\" L,Sz 10,PR",
  "5A228": "Threadlocker 242,10mL Bottle,Blue",
  "22UH80": "Split Lock Washer,Bolt 3/8,Steel,PK50",
  "436A71": "Cut-Resistant Sleeve,A4,18\\\"",
  "2HYJ1": "Coin Battery,Lithium,3VDC,2032,PK2"
}
```