import os
import pickle
import numpy as np

# import uvicorn

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


def load_pickle(path):
    with open(path, "rb") as file:
        df = pickle.load(file)
    return df


@app.get("/")
def index():
    """
    This is a first docstring.
    """
    return {"message": "Hello, User YC"}


@app.post("/predict")
def predict_species(sapAccountId: int, num: int = 10):
    model = load_pickle("app/model/ks_model.pkl")  # 800096562, 886064567
    try:
        pred = {}
        # thred = 1
        for key, val in model[int(sapAccountId)].items():
            if len(pred) < int(num):
                pred[key] = val
            # thred += 1
        return pred
    except KeyError:
        pred = {
            "Result": "Account not covered in model January KeepstockRecommendationsUpsert.csv"
        }
    return pred


# if __name__ == '__main__':
#     uvicorn.run(app, host='0.0.0.0', port=6008)
