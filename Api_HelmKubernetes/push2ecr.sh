aws ecr get-login-password --region us-east-2 --profile prod | docker login --username AWS --password-stdin 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo

docker tag keepstockapi_image:v8 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:keepstockapi_image_v8

docker push 709741256416.dkr.ecr.us-east-2.amazonaws.com/aad-yc-images-repo:keepstockapi_image_v8
