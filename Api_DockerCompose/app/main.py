import pickle
# import uvicorns
import numpy as np
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


def load_pickle(path):
    with open(path, "rb") as file:
        df = pickle.load(file)
    return df


@app.get('/')
def index():
    '''
    This is a first docstring.
    '''
    return {'message': 'Hello, User'}


@app.post('/predict')
def predict_species(sapAccountId: int, num: int = 10):
    model = load_pickle("app/ks_model.pkl")  # 800096562, 886064567
    try:
        pred = {}
        thred = 1
        for key, val in model[int(sapAccountId)].items():
            if len(pred) < int(num):
                pred[key] = val
            thred += 1
        return pred
    except KeyError:
        pred = {"Result": "Account not covered in model January KeepstockRecommendationsUpsert.csv"}
    return pred


# 4. Run the API with uvicorn
#    Will run on http://127.0.0.1:8000
# if __name__ == '__main__':
#     uvicorn.run(app, host='127.0.0.1', port=8000)